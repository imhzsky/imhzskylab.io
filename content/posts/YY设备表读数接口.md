---
title: "YY设备表读数接口"
date: 2021-06-21T17:08:16+08:00
draft: false
description: "YY方案数据接口文档。"
tags:
- UploadData
- SetDownloadParam
- api
categories:
- api
---

因不同的远传水表对数据要求不同，使得每次接入新的远传表时都得写一套对应的代码，造成维护成本高，效率低的情况，所以我司整理出的一套表读数接口对接标准，以此来降低接口对接成本，提高代码可维护性和工作效率；

未经授权请勿使用!
<!--more-->

# 目录

1. 授权
2. 查询获取设备当前数据接口
3. 下行参数接口

# 1.授权



## 接口功能

> 获取调用数据接口的 **access_token**

## URL

> **地址** : https://yywlwebapi.cjlu.edu.cn/token

## 支持格式

> JSON


## HTTP请求方式

> POST

## 请求参数

| 参数       | 必选 | 类型   | 说明               |
| ---------- | ---- | ------ | ------------------ |
| grant_type | true | string | 授权类型，password |
| username   | true | string | 用户名             |
| password   | true | string | 密码               |

## 返回字段

| **返回字段**   | **字段类型** | **说明**                  |
| -------------- | ------------ | ------------------------- |
| *access_token* | **string**   | `access_token，有效2小时` |
| token_type     | string       | token类型，bearer         |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/token

#### 提交参数：

```javascript
curl --location --request POST 'https://yywlwebapi.cjlu.edu.cn/token' \
  --header 'Authorization: ' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=password' \
  --data-urlencode 'username=***' \
  --data-urlencode 'password=***'
```

#### 返回：

```json
    {
      "access_token": "_xkXrh7lxLDu6UYDCDYx0fLWAZj64wGgzANzfKwdcERXpUp6Zh51eo8h3AYLI19TXaE3_c7qQwrJabfbWqN0_YSdCFqn2WYVjFjA9QovV0aZxfVDp3EFih0eNXnQ38Vl6sFq7oZ1wkuzX4J1WHYGJJMAcCwVgt2GXGmmbSlODAnNFdEsP-WvAw_I5TwQ57Xc6QfXDF_z1g6LBcN22A7JzYZMXlMZR3THVNho0vqQajZjPtg3dF0Fj4mNEaM4S9NE4u4iK8q_kalYgHhH-I3ZGXBbvufKUoylu9hZfjiV5MZfVlDBRKx7gWZN34ToY2HI191ZsHO0pazKvQ4JehRgaUuXFIyROdNYxMZRonmPOZuZ262hs_2m58eKUm_xQJaOmbD8fJbxE1UH2pDkjJq65g",
      "token_type": "bearer",
      "expires_in": 7199,
      "as:client_id": "",
      "userName": "***",
      ".issued": "Thu, 06 Aug 2020 01:47:02 GMT",
      ".expires": "Thu, 06 Aug 2020 03:47:02 GMT"
    }
```

# 2. 查询获取设备当前数据



## 接口功能



获取设备当前数据信息



## URL



> **地址** ：https://yywlwebapi.cjlu.edu.cn/api/UploadData/GetQZJYYUploadData



## 支持格式

JSON

# HTTP请求方式

POST

## 请求参数

| **参数**     | **必选** | **类型** | **说明**                                                     |
| ------------ | -------- | -------- | ------------------------------------------------------------ |
| start        | false    | string   | 页码$\times$每页数据条目数                                   |
| length       | false    | string   | 每页数据条目数                                               |
| search.value | false    | string   | 筛选条件，以分号分隔：分别为：”表号”;”厂家代码”;”表类型”;”开始时间”;”结束时间” |

## 返回字段

| **返回字段**          | **字段类型** | **说明**                              |
| --------------------- | ------------ | ------------------------------------- |
| success               | bool         | 返回结果状态。true：正常；false：异常 |
| recordsTotal          | int          | 数据条目数                            |
| recordsFiltered       | int          | 经过条件筛选后数据条目数              |
| data.Nameplate        | string       | 设备号                                |
| data.MfrsNo           | string       | 厂家号                                |
| data.UpTime           | string       | 上传时间                              |
| data.SignalIntensity  | string       | 信号强度（0-31）                      |
| data.BatteryVoltage   | string       | 电池电压（mv）                        |
| data.TotalConsumption | string       | 累计用量                              |
| data.Status           | string       | 设备状态                              |
| data.MeterTime        | string       | 设备时间                              |
| data.ValveStatus      | string       | 设备运行时间                          |
| data.DecimalDigit     | string       | 小数位数                              |
| data.ValveRecord      | string       | 最近四次阀门状态，及其原因            |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/api/UploadData/GetQZJYYUploadData

提交参数：

```json
{
  "start": 0,
  "length": 5,
  "search": {
    "value": ";;;;"
  }
}
```

返回：

```json
{
  "success": true,
  "msg": "",
  "draw": 0,
  "recordsTotal": 22748,
  "recordsFiltered": 22748,
  "data": [
    {
      "Nameplate": "FFFFFFFF",
      "MfrsNo": "105",
      "UpTime": "2021-07-30T14:41:27.097",
      "SignalIntensity": 23,
      "BatteryVoltage": 3585,
      "TotalConsumption": 0,
      "Status": "0",
      "MeterTime": "2117-02-07T06:28:49",
      "ValveStatus": "0",
      "DecimalDigit": "4",
      "ValveRecord": "2000/1/1 0:00:00=0;2000/1/1 0:00:00=0;2000/1/1 0:00:00=0;2000/1/1 0:00:00=0;",
      "RawDataID": 17328255
    },
    {
      "Nameplate": "20156041",
      "MfrsNo": "4",
      "UpTime": "2021-07-30T14:41:26.997",
      "SignalIntensity": 16,
      "BatteryVoltage": 5100,
      "TotalConsumption": 4545,
      "Status": "16",
      "MeterTime": "2021-07-30T14:41:25",
      "ValveStatus": "0",
      "DecimalDigit": "0",
      "ValveRecord": "2021/5/23 10:43:54=50101;2000/1/1 0:00:26=50101;2021/6/20 11:51:57=50007;2021/5/23 10:44:11=50007;",
      "RawDataID": 17328254
    },
    {
      "Nameplate": "44000006",
      "MfrsNo": "4",
      "UpTime": "2021-07-30T14:41:26.27",
      "SignalIntensity": 22,
      "BatteryVoltage": 5004,
      "TotalConsumption": 4893,
      "Status": "0",
      "MeterTime": "2021-07-30T14:41:25",
      "ValveStatus": "0",
      "DecimalDigit": "0",
      "ValveRecord": "2021/6/20 12:00:23=50007;2021/7/25 20:18:52=50007;2021/7/25 20:23:31=50101;2021/6/16 23:27:15=50101;",
      "RawDataID": 17328253
    },
    {
      "Nameplate": "21150591",
      "MfrsNo": "4",
      "UpTime": "2021-07-30T14:41:23.147",
      "SignalIntensity": 17,
      "BatteryVoltage": 5148,
      "TotalConsumption": 0.13,
      "Status": "0",
      "MeterTime": "2021-07-30T14:41:18",
      "ValveStatus": "0",
      "DecimalDigit": "2",
      "ValveRecord": "2000/1/1 0:00:34=50101;2021/7/13 15:49:39=50007;2000/1/1 0:00:25=50101;2021/7/29 13:02:12=50007;",
      "RawDataID": 17328252
    },
    {
      "Nameplate": "26000219",
      "MfrsNo": "105",
      "UpTime": "2021-07-30T14:41:20.457",
      "SignalIntensity": 13,
      "BatteryVoltage": 2948,
      "TotalConsumption": 63.7284,
      "Status": "72",
      "MeterTime": "2021-07-30T14:40:27",
      "ValveStatus": "1",
      "DecimalDigit": "4",
      "ValveRecord": "2000/1/1 0:00:00=0;2000/1/1 0:00:00=0;2000/1/1 0:00:00=0;2000/1/1 0:00:00=0;",
      "RawDataID": 17328251
    }
  ]
}
```

# 3.创建设备的下行参数

## 接口功能

创建设备的下行参数

## URL

> **地址** ：https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/SetParameters

## 支持格式

JSON

## HTTP请求方式

POST

## 请求参数

| 参数                                             | 必选  | 字段类型 | 说明                                        |
| ------------------------------------------------ | ----- | -------- | ------------------------------------------- |
| devicesList                                      | true  | json     | 要下参的设备列表                            |
| devicesList.nameplate                            | true  | string   | 设备号                                      |
| devicesList.mfrsNo                               | true  | string   | 厂家号                                      |
| downLoadParam                                    | false | json     | 下行参数                                    |
| downLoadParam.apn                                | false | string   | APN                                         |
| downLoadParam.masterIP                           | false | integer  | 主用IP                                      |
| downLoadParam.slaveIP                            | false | integer  | 备用IP                                      |
| downLoadParam.tcpPort                            | false | integer  | 端口号                                      |
| downLoadParam.daysOfCommFailToCloseValve         | false | integer  | 连续通信失败关阀天数                        |
| downLoadParam.daysOfUnusedToCloseValve           | false | integer  | 连续未使用关阀天数                          |
| downLoadParam.paramOfValveControl                | false | integer  | 阀门控制参数，0：开阀；1：关阀；2：阀门半开 |
| downLoadParam.meterUserStatus                    | false | integer  | 表用户状态                                  |
| downLoadParam.pointOfLagreFlowLimitPulseInterval | false | integer  | 大流限脉冲间隔分界点                        |
| downLoadParam.pointOfSmallFlowLimitPulseInterval | false | integer  | 小流限脉冲间隔分界点                        |
| downLoadParam.noOfLargeFlowAbnormalPulse         | false | integer  | 大流异常流量脉冲个数                        |
| downLoadParam.noOfSmallFlowAbnormalPulse         | false | integer  | 小流异常流量脉冲个数                        |
| downLoadParam.dateOfMonthlyConsumptionCount      | false | string   | 月用量统计日                                |
| downLoadParam.hoursOfCommInterval                | false | string   | 通信间隔小时数                              |
| downLoadParam.uploadTimeOfEveryday               | false | integer  | 每日开始传输时间                            |
| downLoadParam.lengthOfTransmissionSection        | false | integer  | 传输区间时长                                |
| downLoadParam.transmissionTimeOffset             | false | integer  | 传输偏移时间                                |
| downLoadParam.valveOpenTimeAfterLargeFlow        | false | integer  | 超大流量后允许开阀时间                      |
| downLoadParam.decimalDigitCtr                    | false | integer  | 设置小数位                                  |
| downLoadParam.clearCommand                       | false | integer  | 清零                                        |
| downLoadParam.limitTimesOfDailykeyNetworking     | false | integer  | 日按键次数限制                              |
| downLoadParam.CumulativePurchases                | false | integer  | 累计购买量/出厂预设量                       |

## 返回字段

| **返回字段**             | **字段类型** | **说明**                              |
| ------------------------ | ------------ | ------------------------------------- |
| success                  | bool         | 返回结果状态。true：正常；false：错误 |
| data.successedCount      | string       | 成功创建下行参数设备数量              |
| data.abortedCount        | string       | 创建下行参数失败设备数量              |
| data.abortedMessageLists | string[]     | 创建下行参数失败设备信息              |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/SetParameters

提交参数（关阀）：

```json
{
  "devicesList": [
  {
    "nameplate": "22334455",
    "mfrsno": "4"
  }],
  "paramOfValveControl": "1"
  }
```

返回：

```json
{
  "success": true,
  "data": {
    "successedCount": 1,
    "abortedCount": 0,
    "abortedMessageLists": []
  }
}
```
