---
author: "SKY"
title: "VCP设备上下行数据接口"
date: "2021-06-20T23:50:14+08:00"
draft: false
description: "Sample article showcasing basic Markdown syntax and formatting for HTML elements."
tags:
- uploaddata
- BatchSetDownParam
- api
categories:
- api
---

本接口文档用于物联网设备的上下行数据方法参考，未经授权请勿使用!
<!--more-->

# 目录

1. 授权
2. 查询获取设备当前数据接口
3. 下行参数接口

# 1.授权



## 接口功能

> 获取调用数据接口的 **access_token**

## URL

> **地址** : https://yywlwebapi.cjlu.edu.cn/token

## 支持格式

> JSON


## HTTP请求方式

> POST

## 请求参数

 参数 | 必选 | 类型 | 说明 
--------|------|--------|--------
 grant_type | true | string | 授权类型，password 
 username | true | string | 用户名 
 password | true | string | 密码 

## 返回字段

| **返回字段**   | **字段类型**| **说明**                  |
| -------------- | ------------ | ------------------------- |
| *access_token* | **string**   | `access_token，有效2小时` |
| token_type     | string       | token类型，bearer         |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/token

#### 提交参数：

```javascript
curl --location --request POST 'https://yywlwebapi.cjlu.edu.cn/token' \
  --header 'Authorization: ' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=password' \
  --data-urlencode 'username=***' \
  --data-urlencode 'password=***'
```

#### 返回：

```json
    {
      "access_token": "_xkXrh7lxLDu6UYDCDYx0fLWAZj64wGgzANzfKwdcERXpUp6Zh51eo8h3AYLI19TXaE3_c7qQwrJabfbWqN0_YSdCFqn2WYVjFjA9QovV0aZxfVDp3EFih0eNXnQ38Vl6sFq7oZ1wkuzX4J1WHYGJJMAcCwVgt2GXGmmbSlODAnNFdEsP-WvAw_I5TwQ57Xc6QfXDF_z1g6LBcN22A7JzYZMXlMZR3THVNho0vqQajZjPtg3dF0Fj4mNEaM4S9NE4u4iK8q_kalYgHhH-I3ZGXBbvufKUoylu9hZfjiV5MZfVlDBRKx7gWZN34ToY2HI191ZsHO0pazKvQ4JehRgaUuXFIyROdNYxMZRonmPOZuZ262hs_2m58eKUm_xQJaOmbD8fJbxE1UH2pDkjJq65g",
      "token_type": "bearer",
      "expires_in": 7199,
      "as:client_id": "",
      "userName": "***",
      ".issued": "Thu, 06 Aug 2020 01:47:02 GMT",
      ".expires": "Thu, 06 Aug 2020 03:47:02 GMT"
    }
```

# 2. 查询获取设备当前数据



## 接口功能



获取设备当前数据信息



## URL



> **地址** ：https://yywlwebapi.cjlu.edu.cn/api/VCPUploadData/GetVCPUploadData
>
> **或：** https://yywlwebapi.cjlu.edu.cn/api/VCPUploadData/GetQZJVCPUploadData



## 支持格式

JSON

# HTTP请求方式

POST

## 请求参数

| **参数**     | **必选** | **类型** | **说明**                                                     |
| ------------ | -------- | -------- | ------------------------------------------------------------ |
| start        | false    | string   | 页码$\times$每页数据条目数                                   |
| length       | false    | string   | 每页数据条目数；为空时默认为1000，超出1000个设备时注意需要提供分页参数 |
| search.value | false    | string   | 筛选条件，以分号分隔：分别为：”表号（多个表号“,”分隔）”;”厂家代码”;”表类型”;”开始时间”;”结束时间” |
| datatype     | false    | string   | 数据类别：查询设备当前数据为空；查询设备历史数据时为：”UploadDataHis” |

## 返回字段

| **返回字段**        | **字段类型** | **说明**                              |
| ------------------- | ------------ | ------------------------------------- |
| success             | bool         | 返回结果状态。true：正常；false：异常 |
| recordsTotal        | int          | 数据条目数                            |
| recordsFiltered     | int          | 经过条件筛选后数据条目数              |
| data.MID            | string       | 设备号                                |
| data.FID            | string       | 厂家号                                |
| data.Upload_Time    | string       | 上传时间                              |
| data.CSQ            | string       | 信号强度                              |
| data.BatteryVoltage | string       | 电池电压（mv）                        |
| data.CumuConsum     | string       | 累计用量                              |
| data.MeterStatus    | string       | 设备状态                              |
| data.MeterTime      | string       | 设备时间                              |
| data.RunningTime    | string       | 设备运行时间                          |
| data.UploadReason   | string       | 上传原因                              |
| data.DecimalDigit   | string       | 小数位数                              |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/api/VCPUploadData/GetVCPUploadData
>
> **或：** https://yywlwebapi.cjlu.edu.cn/api/VCPUploadData/GetQZJVCPUploadData

提交参数：

```json
{
  "start": 0,
  "length": 5,
  "search": {
    "value": "21040015,21040021;;;;"
  },
  "datatype": "UploadDataHis"
}
```

返回：

```json
{
  "success": true,
  "msg": "",
  "draw": 0,
  "recordsTotal": 36,
  "recordsFiltered": 36,
  "data": [
    {
      "MID": "21040015",
      "FID": "104",
      "Upload_Time": "2021-06-21T17:01:27",
      "CSQ": "13",
      "BatteryVoltage": 3516,
      "CumuConsum": 2650301,
      "MeterStatus": "81",
      "MeterTime": 677610084,
      "RunningTime": 1817192,
      "UploadReason": "10",
      "DecimalDigit": "4",
      "RawDataID": 16014571
    },
    {
      "MID": "21040021",
      "FID": "104",
      "Upload_Time": "2021-06-21T17:00:45",
      "CSQ": "20",
      "BatteryVoltage": 3505,
      "CumuConsum": 2731274,
      "MeterStatus": "0",
      "MeterTime": 677610043,
      "RunningTime": 1818200,
      "UploadReason": "10",
      "DecimalDigit": "4",
      "RawDataID": 16014563
    },
    {
      "MID": "21040018",
      "FID": "104",
      "Upload_Time": "2021-06-21T16:59:49",
      "CSQ": "16",
      "BatteryVoltage": 3580,
      "CumuConsum": 2453665,
      "MeterStatus": "0",
      "MeterTime": 677609985,
      "RunningTime": 1811100,
      "UploadReason": "10",
      "DecimalDigit": "4",
      "RawDataID": 16014550
    },
    {
      "MID": "21040016",
      "FID": "104",
      "Upload_Time": "2021-06-21T16:58:24",
      "CSQ": "12",
      "BatteryVoltage": 3537,
      "CumuConsum": 3512098,
      "MeterStatus": "1",
      "MeterTime": 677609902,
      "RunningTime": 1825605,
      "UploadReason": "10",
      "DecimalDigit": "4",
      "RawDataID": 16014536
    },
    {
      "MID": "26000101",
      "FID": "104",
      "Upload_Time": "2021-06-21T16:02:35",
      "CSQ": "10",
      "BatteryVoltage": 3593,
      "CumuConsum": 4557,
      "MeterStatus": "0",
      "MeterTime": 677606449,
      "RunningTime": 1807644,
      "UploadReason": "10",
      "DecimalDigit": "4",
      "RawDataID": 16013950
    }
  ]
}
```

# 3.创建设备的下行参数

## 接口功能

创建设备的下行参数

## URL

> **地址** ：https://yywlwebapi.cjlu.edu.cn/api/VCPUploadData/BatchSetDownParam

## 支持格式

JSON

## HTTP请求方式

POST

## 请求参数

| 参数                             | 必选  | 字段类型 | 说明                   |
| -------------------------------- | ----- | -------- | ---------------------- |
| devicesList                      | true  | json     | 要下参的设备列表       |
| devicesList.nameplate            | true  | string   | 设备号                 |
| devicesList.mfrsNo               | true  | string   | 厂家号                 |
| downLoadParam                    | false | json     | 下行参数               |
| downLoadParam.csCheckNoUsedTime  | false | string   | 关怀用水开始时段       |
| downLoadParam.csCheckNoUsedHours | false | integer  | 关怀用水时段长度       |
| downLoadParam.csCheckNoUsedTH    | false | integer  | 关怀用水阈值           |
| downLoadParam.cLongUsedHoursTH   | false | integer  | 长时间用水时长阈值     |
| downLoadParam.cMinFlowTH         | false | integer  | 最低流速阈值           |
| downLoadParam.nLongUsedFlowTH    | false | integer  | 长时间用水流速阈值     |
| downLoadParam.nNoPowerTH         | false | integer  | 低压报警阈值           |
| downLoadParam.nNosFlowTH         | false | integer  | 反流报警阈值           |
| downLoadParam.nMaxFlowTH         | false | integer  | 大流异常流速阈值       |
| downLoadParam.nMinFlowTH         | false | integer  | 小流异常流速阈值       |
| downLoadParam.cBigLimitSec       | false | integer  | 大流异常时间阈值（秒） |
| downLoadParam.cMiniLimitMin      | false | integer  | 小流异常时间阈值（分） |
| downLoadParam.csIPAddress        | false | string   | 主IP                   |
| downLoadParam.csIPAddress2       | false | string   | 备用IP                 |
| downLoadParam.nPort              | false | integer  | 端口号                 |
| downLoadParam.cOffLineDays       | false | integer  | 发送失败关闭计数       |
| downLoadParam.cNoUsedCloswDay    | false | integer  | 连续未用关阀天数       |
| downLoadParam.cSwCtrl            | false | integer  | 阀门控制参数           |
| downLoadParam.cSendPHours        | false | integer  | 发送间隔小时           |
| downLoadParam.cSendStartHours    | false | integer  | 每日开始传输的时间     |
| downLoadParam.cSendSpaceHours    | false | integer  | 传输区间时长           |
| downLoadParam.cNetType           | false | integer  | 联网类型               |
| downLoadParam.cDotNum            | false | integer  | 小数位设置             |
| downLoadParam.cCmd               | false | string   | 清零指令               |
| downLoadParam.cDayKeyTimes       | false | integer  | 按键限制               |
| downLoadParam.lMeterBase         | false | integer  | 读数修正               |

## 返回字段

| **返回字段**             | **字段类型** | **说明**                              |
| ------------------------ | ------------ | ------------------------------------- |
| success                  | bool         | 返回结果状态。true：正常；false：错误 |
| data.successedCount      | string       | 成功创建下行参数设备数量              |
| data.abortedCount        | string       | 创建下行参数失败设备数量              |
| data.abortedMessageLists | string[]     | 创建下行参数失败设备信息              |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/api/VCPUploadData/BatchSetDownParam

提交参数（关阀）：

```json
{
  "devicesList": [
    {
      "nameplate": "21040022",
      "mfrsNo": "104"
    }
  ],
  "downLoadParam": {
    "cSwCtrl": 1
  }
}
```

返回：

```json
{
  "success": true,
  "data": {
    "successedCount": 1,
    "abortedCount": 0,
    "abortedMessageLists": []
  }
}
```
