
---
title: "YY设备上下行数据接口"
date: 2021-06-21T17:08:16+08:00
draft: false
description: "YY方案数据接口文档。"
tags:
- UploadData
- SetDownloadParam
- api
categories:
- api
---

本接口文档用于物联网设备的上下行数据方法参考，未经授权请勿使用!
<!--more-->

# 目录

1. 授权
2. 查询获取设备当前数据接口
3. 下行参数接口

# 1.授权



## 接口功能

> 获取调用数据接口的 **access_token**

## URL

> **地址** : https://yywlwebapi.cjlu.edu.cn/token

## 支持格式

> JSON


## HTTP请求方式

> POST

## 请求参数

| 参数       | 必选 | 类型   | 说明               |
| ---------- | ---- | ------ | ------------------ |
| grant_type | true | string | 授权类型，password |
| username   | true | string | 用户名             |
| password   | true | string | 密码               |

## 返回字段

| **返回字段**   | **字段类型** | **说明**                  |
| -------------- | ------------ | ------------------------- |
| *access_token* | **string**   | `access_token，有效2小时` |
| token_type     | string       | token类型，bearer         |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/token

#### 提交参数：

```javascript
curl --location --request POST 'https://yywlwebapi.cjlu.edu.cn/token' \
  --header 'Authorization: ' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=password' \
  --data-urlencode 'username=***' \
  --data-urlencode 'password=***'
```

#### 返回：

```json
    {
      "access_token": "_xkXrh7lxLDu6UYDCDYx0fLWAZj64wGgzANzfKwdcERXpUp6Zh51eo8h3AYLI19TXaE3_c7qQwrJabfbWqN0_YSdCFqn2WYVjFjA9QovV0aZxfVDp3EFih0eNXnQ38Vl6sFq7oZ1wkuzX4J1WHYGJJMAcCwVgt2GXGmmbSlODAnNFdEsP-WvAw_I5TwQ57Xc6QfXDF_z1g6LBcN22A7JzYZMXlMZR3THVNho0vqQajZjPtg3dF0Fj4mNEaM4S9NE4u4iK8q_kalYgHhH-I3ZGXBbvufKUoylu9hZfjiV5MZfVlDBRKx7gWZN34ToY2HI191ZsHO0pazKvQ4JehRgaUuXFIyROdNYxMZRonmPOZuZ262hs_2m58eKUm_xQJaOmbD8fJbxE1UH2pDkjJq65g",
      "token_type": "bearer",
      "expires_in": 7199,
      "as:client_id": "",
      "userName": "***",
      ".issued": "Thu, 06 Aug 2020 01:47:02 GMT",
      ".expires": "Thu, 06 Aug 2020 03:47:02 GMT"
    }
```

# 2. 查询获取设备当前数据



## 接口功能



获取设备当前数据信息



## URL



> **地址** ：https://yywlwebapi.cjlu.edu.cn/api/UploadData/GetQZJYYUploadData



## 支持格式

JSON

# HTTP请求方式

POST

## 请求参数

| **参数**     | **必选** | **类型** | **说明**                                                     |
| ------------ | -------- | -------- | ------------------------------------------------------------ |
| start        | false    | string   | 页码乘以每页数据条目数                                   |
| length       | false    | string   | 每页数据条目数                                               |
| search.value | false    | string   | 筛选条件，以分号分隔：分别为：”表号”;”厂家代码”;”表类型”;”开始时间”;”结束时间” |
| datatype    | false    | string   | 数据类型：当前数据为空，“UploadDataHis”为历史数据

## 返回字段

| **返回字段**              | **字段类型** | **说明**                                                     |
| ------------------------- | ------------ | ------------------------------------------------------------ |
| success                   | bool         | 返回结果状态。true：正常；false：异常                        |
| recordsTotal              | int          | 数据条目数                                                   |
| recordsFiltered           | int          | 经过条件筛选后数据条目数                                     |
| data.Nameplate            | string       | 设备号                                                       |
| data.MfrsNo               | string       | 厂家号                                                       |
| data.UpTime               | string       | 上传时间                                                     |
| data.SignalIntensity      | string       | 信号强度（0-31）                                             |
| data.BatteryVoltage       | string       | 电池电压（mv）                                               |
| data.TotalConsumption     | string       | 累计用量                                                     |
| data.Status               | string       | 设备状态                                                     |
| data.MeterTime            | string       | 设备时间                                                     |
| data.ValveStatus          | string       | 阀门状态                                                     |
| data.DecimalDigit         | string       | 小数位数                                                     |
| data.ValveRecord          | string       | 最近四次阀门状态，及其原因                                   |
| data.ExceptionRecord      | string       | 最近四次异常状态时间及异常代码（“;“分割）：<br/>0 无异常<br/>1 掉电复位，2 看门狗复位，3 程序异常复位<br/>4~9 厂家自定义<br/>10 磁干扰（脉冲）<br/>11 超大流量<br/>12 超小流量<br/>13 采样失败(直读TTL或无磁采样TTL)<br/>20 通信模块无响应，<br/>21 无SIM卡，<br/>26 注册网络超时<br/>28 连接中断<br/>29 服务器无响应<br/>32 阀门断路，33 阀门短路或卡死，34 开关阀超时<br/>103 数据丢失（严重故障） |
| data.UltralSmallFlow      | bool         | 超小流量：1有，0没有                                         |
| data.OverFlow             | bool         | 超大流量：1有，0没有                                         |
| data.BatteryStatus        | bool         | 电池欠压：1有，0没有                                         |
| data.ThroughUse           | bool         | 直通：1有，0没有                                             |
| data.MagneticInterference | bool         | 磁攻击：1有，0没有                                           |
| data.RemoteValveClosing   | bool         | 远程关阀：1有，0没有                                         |
| data.SamplingFalure       | bool         | 采样失败：1有，0没有                                         |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/api/UploadData/GetQZJYYUploadData

提交参数：

```json
{
  "start": 0,
  "length": 5,
  "search": {
    "value": ";;;;"
  }
}
```

返回：

```json
{
  "success": true,
  "msg": "",
  "draw": 0,
  "recordsTotal": 26164,
  "recordsFiltered": 26164,
  "data": [
    {
      "Nameplate": "21150566",
      "MfrsNo": "4",
      "UpTime": "2021-08-16T14:44:53.383",
      "SignalIntensity": 20,
      "BatteryVoltage": 5208,
      "TotalConsumption": 2,
      "Status": "8",
      "MeterTime": "2021-08-16T14:44:52",
      "ValveStatus": "1",
      "DecimalDigit": "0",
      "ValveRecord": "2021/7/17 22:46:05=50101;2021/7/18 13:00:15=50007;2021/7/17 22:42:47=50007;2021/8/16 11:08:47=50011;",
      "ExceptionRecord": "2021/7/18 14:51:07=29;2021/7/18 13:00:00=1;2021/7/18 13:01:22=24;2021/7/18 14:17:28=24;",
      "UltralSmallFlow": false,
      "OverFlow": false,
      "BatteryStatus": false,
      "ThroughUse": false,
      "MagneticInterference": false,
      "RemoteValveClosing": false,
      "SamplingFalure": false,
      "RawDataID": 18094814
    },
    {
      "Nameplate": "21150690",
      "MfrsNo": "4",
      "UpTime": "2021-08-16T14:44:52.047",
      "SignalIntensity": 10,
      "BatteryVoltage": 5202,
      "TotalConsumption": 0.24,
      "Status": "8",
      "MeterTime": "2021-08-16T14:44:37",
      "ValveStatus": "1",
      "DecimalDigit": "2",
      "ValveRecord": "2021/8/7 10:20:33=50007;2021/8/5 7:15:40=50007;2021/8/16 11:08:04=50011;2021/8/7 10:21:48=50101;",
      "ExceptionRecord": "2021/8/16 10:40:16=24;2021/8/16 10:09:40=24;2021/8/16 10:11:34=24;2021/8/16 11:03:29=29;",
      "UltralSmallFlow": false,
      "OverFlow": false,
      "BatteryStatus": false,
      "ThroughUse": false,
      "MagneticInterference": false,
      "RemoteValveClosing": false,
      "SamplingFalure": false,
      "RawDataID": 18094813
    },
    {
      "Nameplate": "20156041",
      "MfrsNo": "4",
      "UpTime": "2021-08-16T14:44:51.303",
      "SignalIntensity": 16,
      "BatteryVoltage": 5097,
      "TotalConsumption": 5018,
      "Status": "16",
      "MeterTime": "2021-08-16T14:44:49",
      "ValveStatus": "0",
      "DecimalDigit": "0",
      "ValveRecord": "2021/5/23 10:43:54=50101;2000/1/1 0:00:26=50101;2021/6/20 11:51:57=50007;2021/5/23 10:44:11=50007;",
      "ExceptionRecord": "2021/8/10 16:49:24=29;2021/8/2 8:56:57=29;2021/8/16 11:02:13=29;2021/8/3 13:48:09=29;",
      "UltralSmallFlow": false,
      "OverFlow": false,
      "BatteryStatus": false,
      "ThroughUse": true,
      "MagneticInterference": false,
      "RemoteValveClosing": false,
      "SamplingFalure": false,
      "RawDataID": 18094812
    },
    {
      "Nameplate": "21150576",
      "MfrsNo": "4",
      "UpTime": "2021-08-16T14:44:51.267",
      "SignalIntensity": 29,
      "BatteryVoltage": 5258,
      "TotalConsumption": 12,
      "Status": "8",
      "MeterTime": "2021-08-16T14:44:49",
      "ValveStatus": "1",
      "DecimalDigit": "0",
      "ValveRecord": "2021/8/16 11:06:39=50011;2021/7/18 14:23:49=50007;2021/7/18 1:46:32=50007;2021/7/17 16:15:02=50007;",
      "ExceptionRecord": "2021/7/18 8:06:17=24;2021/7/18 14:00:00=1;2021/7/18 19:02:30=29;2021/7/18 5:23:04=24;",
      "UltralSmallFlow": false,
      "OverFlow": false,
      "BatteryStatus": false,
      "ThroughUse": false,
      "MagneticInterference": false,
      "RemoteValveClosing": false,
      "SamplingFalure": false,
      "RawDataID": 18094811
    },
    {
      "Nameplate": "21150051",
      "MfrsNo": "4",
      "UpTime": "2021-08-16T14:44:47.33",
      "SignalIntensity": 23,
      "BatteryVoltage": 5192,
      "TotalConsumption": 0.08,
      "Status": "8",
      "MeterTime": "2021-08-16T14:44:25",
      "ValveStatus": "1",
      "DecimalDigit": "2",
      "ValveRecord": "2021/8/8 10:57:26=50007;2021/8/8 10:58:55=50101;2021/8/6 7:12:22=50007;2021/8/16 11:07:36=50011;",
      "ExceptionRecord": "2021/8/15 12:16:39=24;2021/8/16 3:32:57=24;2021/8/16 6:50:37=24;2021/8/16 10:55:37=29;",
      "UltralSmallFlow": false,
      "OverFlow": false,
      "BatteryStatus": false,
      "ThroughUse": false,
      "MagneticInterference": false,
      "RemoteValveClosing": false,
      "SamplingFalure": false,
      "RawDataID": 18094810
    }
  ]
}
```

# 3.创建设备的下行参数

## 接口功能

创建设备的下行参数

## URL

> **地址** ：https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/SetParameters

## 支持格式

JSON

## HTTP请求方式

POST

## 请求参数

| 参数                                             | 必选  | 字段类型 | 说明                                        |
| ------------------------------------------------ | ----- | -------- | ------------------------------------------- |
| devicesList                                      | true  | json     | 要下参的设备列表                            |
| devicesList.nameplate                            | true  | string   | 设备号                                      |
| devicesList.mfrsNo                               | true  | string   | 厂家号                                      |
| downLoadParam                                    | false | json     | 下行参数                                    |
| downLoadParam.apn                                | false | string   | APN                                         |
| downLoadParam.masterIP                           | false | integer  | 主用IP                                      |
| downLoadParam.slaveIP                            | false | integer  | 备用IP                                      |
| downLoadParam.tcpPort                            | false | integer  | 端口号                                      |
| downLoadParam.daysOfCommFailToCloseValve         | false | integer  | 连续通信失败关阀天数                        |
| downLoadParam.daysOfUnusedToCloseValve           | false | integer  | 连续未使用关阀天数                          |
| downLoadParam.paramOfValveControl                | false | integer  | 阀门控制参数，0：开阀；1：关阀；2：阀门半开 |
| downLoadParam.meterUserStatus                    | false | integer  | 表用户状态                                  |
| downLoadParam.pointOfLagreFlowLimitPulseInterval | false | integer  | 大流限脉冲间隔分界点                        |
| downLoadParam.pointOfSmallFlowLimitPulseInterval | false | integer  | 小流限脉冲间隔分界点                        |
| downLoadParam.noOfLargeFlowAbnormalPulse         | false | integer  | 大流异常流量脉冲个数                        |
| downLoadParam.noOfSmallFlowAbnormalPulse         | false | integer  | 小流异常流量脉冲个数                        |
| downLoadParam.dateOfMonthlyConsumptionCount      | false | string   | 月用量统计日                                |
| downLoadParam.hoursOfCommInterval                | false | string   | 通信间隔小时数                              |
| downLoadParam.uploadTimeOfEveryday               | false | integer  | 每日开始传输时间                            |
| downLoadParam.lengthOfTransmissionSection        | false | integer  | 传输区间时长                                |
| downLoadParam.transmissionTimeOffset             | false | integer  | 传输偏移时间                                |
| downLoadParam.valveOpenTimeAfterLargeFlow        | false | integer  | 超大流量后允许开阀时间                      |
| downLoadParam.decimalDigitCtr                    | false | integer  | 设置小数位                                  |
| downLoadParam.clearCommand                       | false | integer  | 清零                                        |
| downLoadParam.limitTimesOfDailykeyNetworking     | false | integer  | 日按键次数限制                              |
| downLoadParam.CumulativePurchases                | false | integer  | 累计购买量/出厂预设量                       |

## 返回字段

| **返回字段**             | **字段类型** | **说明**                              |
| ------------------------ | ------------ | ------------------------------------- |
| success                  | bool         | 返回结果状态。true：正常；false：错误 |
| data.successedCount      | string       | 成功创建下行参数设备数量              |
| data.abortedCount        | string       | 创建下行参数失败设备数量              |
| data.abortedMessageLists | string[]     | 创建下行参数失败设备信息              |

## 接口示例

> **地址：** https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/SetParameters

提交参数（关阀）：

```json
{
  "devicesList": [
  {
    "nameplate": "22334455",
    "mfrsno": "4"
  }],
  "paramOfValveControl": "1"
  }
```

返回：

```json
{
  "success": true,
  "data": {
    "successedCount": 1,
    "abortedCount": 0,
    "abortedMessageLists": []
  }
}
```
