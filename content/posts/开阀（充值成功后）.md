
---
title: "开阀接口（充值完成后）"
date: 2022-06-04T19:56:16+08:00
draft: false
description: "开阀接口（充值完成后）文档。"
tags:
- SetDownloadParam
- PaymentOpenValve
- api
categories:
- api
---

本接口文档用于物联网设备的上下行数据方法参考，**未经授权请勿使用!**
# 目录

1. 授权
2. 开阀接口（充值完成后）

# 1. 授权

## 接口功能

获取调用数据接口的 **access_token**

## URL

>**地址：**[https://yywlwebapi.cjlu.edu.cn/token](https://yywlwebapi.cjlu.edu.cn/token)

## 支持格式

>JSON

## HTTP请求方式

>POST

## 请求参数

|参数        |必选 |类型   | 说明              |
|-----------|-----|------|-------------------|  
|grant_type |ture |string|授权类型，password  |
|username   |ture |string|用户名              |
|password   |ture |string|密码               |

## 返回字段

|返回字段     |字段类型|说明              |
|------------|-------|-----------------------|  
|access_token|string |access_token，有效2小时 |
|token_type  |string |token类型，bearer      |


## 接口示例

>**地址：**[https://yywlwebapi.cjlu.edu.cn/token](https://yywlwebapi.cjlu.edu.cn/token)

提交参数：

      curl --location --request POST 'https://yywlwebapi.cjlu.edu.cn/token' \
      --header 'Authorization: ' \
      --header 'Content-Type: application/x-www-form-urlencoded' \
      --data-urlencode 'grant_type=password' \
      --data-urlencode 'username=***' \
      --data-urlencode 'password=***'
返回：

    {
      "access_token": "_xkXrh7lxLDu6UYDCDYx0fLWAZj64wGgzANzfKwdcERXpUp6Zh51eo8h3AYLI19TXaE3_c7qQwrJabfbWqN0_YSdCFqn2WYVjFjA9QovV0aZxfVDp3EFih0eNXnQ38Vl6sFq7oZ1wkuzX4J1WHYGJJMAcCwVgt2GXGmmbSlODAnNFdEsP-WvAw_I5TwQ57Xc6QfXDF_z1g6LBcN22A7JzYZMXlMZR3THVNho0vqQajZjPtg3dF0Fj4mNEaM4S9NE4u4iK8q_kalYgHhH-I3ZGXBbvufKUoylu9hZfjiV5MZfVlDBRKx7gWZN34ToY2HI191ZsHO0pazKvQ4JehRgaUuXFIyROdNYxMZRonmPOZuZ262hs_2m58eKUm_xQJaOmbD8fJbxE1UH2pDkjJq65g",
      "token_type": "bearer",
      "expires_in": 7199,
      "refresh_token": "abd794c2aa8c40d8818faf44eb3bf714",
      "as:client_id": "",
      "userName": "***",
      ".issued": "Thu, 06 Aug 2020 01:47:02 GMT",
      ".expires": "Thu, 06 Aug 2020 03:47:02 GMT"
    }

# 2. 开阀（充值完成后）

## 接口功能

充值完成后，请求开阀

## URL

> **地址：**[https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/PaymentOpenValve](https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/PaymentOpenValve)

## 支持格式

>JSON

# HTTP请求方式

>POST

## 请求参数


|参数         |必选 |类型   | 说明              |
|------------|-----|------|------------------|  
|nameplate|true |string|表号|
|mfrsno|true |string|厂家代码|

## 返回字段


|返回字段                 |字段类型|说明              |
|------------------------|-------|-----------------------|  
|success|bool|返回结果状态。true：正常；false：错误|
|data.successedCount|string |完成开阀设备数量|
|data.abortedCount|string |开阀失败设备数量|
|data.abortedMessageLists|List |失败信息列表|

## 接口示例

> **地址：**[https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/PaymentOpenValve](https://yywlwebapi.cjlu.edu.cn/api/SetDownloadParam/PaymentOpenValve)

提交参数：

    { 
	    "mid": "12345678", 
	    "fid": "104"
	}

返回：

    { 
	    "success": true, 
	    "data": 
	    { 
		    "successedCount": 1, 
		    "abortedCount": 0, 
		    "abortedMessageLists": [] 
		} 
	}